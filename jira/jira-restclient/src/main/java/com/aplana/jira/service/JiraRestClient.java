package com.aplana.jira.service;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.aplana.jira.JiraConsts;
import com.aplana.jira.entity.AbstractIssue;
import com.aplana.jira.entity.AbstractMeta;
import com.aplana.jira.entity.AbstractSearchResults;
import com.aplana.jira.entity.GroupMembers;
import com.aplana.jira.entity.PostSearchParams;
import com.aplana.jira.entity.User;
import com.aplana.service.AbstractRestClient;

public class JiraRestClient<T extends AbstractIssue, R extends AbstractSearchResults, M extends AbstractMeta> extends AbstractRestClient {
	
    /*--------------------------------------------------------------- Members */
    private static final Logger logger = LoggerFactory.getLogger(JiraRestClient.class);

    private String username;
    private String password;
    
    private String authHeader;
	
    /*------------------------------------------------------------------ Init */
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setSslMode(boolean sslMode) {
		this.sslMode = sslMode;
	}
	
	private String getAuthHeader() {
		if (authHeader == null) {
		    String auth = username + ":" + password;
		    byte[] bytesEncoded = Base64.encodeBase64(auth.getBytes());
		    authHeader = "Basic " + new String(bytesEncoded);
		}
		return authHeader;
	}
	
	protected HttpEntity<String> getRequestEntity() {
    	HttpHeaders headers = new HttpHeaders();
    	headers.set(JiraConsts.JIRA_REST_HEADER_AUTH, getAuthHeader());
    	HttpEntity<String> entity = new HttpEntity<String>(headers);
		return entity;
	}
	
	protected <S> HttpEntity<S> getRequestEntity(S params) {
    	HttpHeaders headers = new HttpHeaders();
    	headers.set(JiraConsts.JIRA_REST_HEADER_AUTH, getAuthHeader());
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<S> entity = new HttpEntity<S>(params, headers);
		return entity;
	}
	
    /*----------------------------------------------------------------- Issue */
    @SuppressWarnings("unchecked")
	public T getIssue(String issueKey, String fields) {
    	RestTemplate template = getRestTemplate();  
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_ISSUE + issueKey + "?fields=" + fields;
    	try {
    		Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            ResponseEntity<T> result = template.exchange(url, HttpMethod.GET, entity, clazz);
            return result.getBody();
    	}
    	catch (HttpClientErrorException e) {
    		if (e.getStatusCode().value() == 404) {
    			return null;
    		}
    		throw e;
    	}
    }
	
    /*------------------------------------------------------------------ Meta */
	@SuppressWarnings("unchecked")
	public M getIssueTypeMeta(String projectKey, String issueType) {
    	RestTemplate template = getRestTemplate();  
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_ISSUETYPE_META + "?projectKeys=" + projectKey + "&issuetypeNames=" + issueType + "&expand=projects.issuetypes.fields";
    	Method method;
    	try {
    	    method = getClass().getMethod("getIssueTypeMeta", String.class, String.class);
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}   	
    	Class<M> clazz = (Class<M>) GenericTypeResolver.resolveReturnType(method, getClass());
    	ResponseEntity<M> result = template.exchange(url, HttpMethod.GET, entity, clazz);
    	return result.getBody();
	}
	
    /*---------------------------------------------------------------- Search */
    @SuppressWarnings("unchecked")
	public R getSearchResults(String jql, int maxResults, String fields) {
    	RestTemplate template = getRestTemplate();  
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_SEARCH + "?fields=" + fields + "&maxResults=" + maxResults + "&jql=" + jql;
    	Method method;
    	try {
    	    method = getClass().getMethod("getSearchResults", String.class, int.class, String.class);
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}   	
    	Class<R> clazz = (Class<R>) GenericTypeResolver.resolveReturnType(method, getClass());
    	ResponseEntity<R> result = template.exchange(url, HttpMethod.GET, entity, clazz);
    	return result.getBody();
    }

    @SuppressWarnings("unchecked")
	public R postSearchResults(String jql, int maxResults, String fields) {
    	RestTemplate template = getRestTemplate();
    	String[] arrFields = fields.split(",");
    	PostSearchParams params = new PostSearchParams(jql, Arrays.asList(arrFields));
    	HttpEntity<PostSearchParams> entity = getRequestEntity(params);
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_SEARCH;
    	Method method;
    	try {
    	    method = getClass().getMethod("postSearchResults", String.class, int.class, String.class);
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}   	
    	Class<R> clazz = (Class<R>) GenericTypeResolver.resolveReturnType(method, getClass());
    	ResponseEntity<R> result = template.exchange(url, HttpMethod.POST, entity, clazz);
    	return result.getBody();
    }
	
    /*------------------------------------------------------------------ User */
    public GroupMembers getGroupMembers(String groupName) {
    	RestTemplate template = getRestTemplate();  
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_GROUP_MEMBER + "?groupname=" + groupName;
    	try {
    	    ResponseEntity<GroupMembers> result = template.exchange(url, HttpMethod.GET, entity, GroupMembers.class);
    	    return result.getBody();
    	}
    	catch (HttpClientErrorException e) {
    		// group not exists
    		if (e.getStatusCode().value() == 404) {
    			return null;
    		}
    		throw e;
    	}
    }
    
    public User getUser(String username, boolean expandGroups) {
    	RestTemplate template = getRestTemplate();
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_USER + "?username=" + username + (expandGroups ? "&expand=groups" : "");
    	try {
    	    ResponseEntity<User> result = template.exchange(url, HttpMethod.GET, entity, User.class);
    	    return result.getBody();
    	}
    	catch (HttpClientErrorException e) {
    		if (e.getStatusCode().value() == 404) {
    			return null;
    		}
    		throw e;
    	}
    }
	
    /*------------------------------------------------------------ Transition */
    public void doTransition(String issueKey, int transitionId) {
    	logger.info("Issue [" + issueKey + "] - transition [" + transitionId + "] started ...");
    	String jsonBody = "{\"transition\": {\"id\": \"" + transitionId + "\"}}";
    	RestTemplate template = getRestTemplate();  
    	HttpHeaders headers = new HttpHeaders();
    	headers.set(JiraConsts.JIRA_REST_HEADER_AUTH, getAuthHeader());
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> entity = new HttpEntity<String>(jsonBody, headers);   	
    	template.exchange(jiraEndpoint + JiraConsts.JIRA_URL_ISSUE + issueKey + "/transitions", HttpMethod.POST, entity, String.class);    	
    	logger.info("Issue [" + issueKey + "] - transition [" + transitionId + "] done");
    }
	
    /*----------------------------------------------------------------- Agile Issue */
    @SuppressWarnings("unchecked")
	public T getAgileIssue(String issueKey, String fields) {
    	RestTemplate template = getRestTemplate();  
    	HttpEntity<String> entity = getRequestEntity();
    	String url = jiraEndpoint + JiraConsts.JIRA_URL_AGILE_ISSUE + issueKey + "?fields=" + fields;
    	try {
    		Class<T> clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            ResponseEntity<T> result = template.exchange(url, HttpMethod.GET, entity, clazz);
            return result.getBody();
    	}
    	catch (HttpClientErrorException e) {
    		if (e.getStatusCode().value() == 404) {
    			return null;
    		}
    		throw e;
    	}
    }

}
