package com.aplana.jira.util;

import java.util.Collection;
import java.util.Date;

import com.aplana.jira.JiraConsts;

/**
 * Служебные методы jira.
 * В том числе обеспечивают функциональность, которая отсутствует в стандартном Jira Rest Api.  
 */
public class JiraClientUtil {
	    
    /*------------------------------------------------------------------- Jql */
	public static String getDateCondition(String field, Date startDate, Date endDate) {
		if (startDate == null && startDate == null) {
			return "";
		}
    	StringBuffer s = new StringBuffer();
		if (startDate != null) {
			s.append(field).append(">=").append("'").append(JiraConsts.FORMAT_JQL_DATE.format(startDate)).append("'");
		}
		if (endDate != null) {
			if (s.length() > 0) {
				s.append(" and ");
			}
			s.append(field).append("<=").append("'").append(JiraConsts.FORMAT_JQL_DATE.format(endDate)).append("'");
		}
    	return s.toString();
	}

	public static String getFieldInCondition(String field, Collection<String> values) {
    	if (values.isEmpty()) {
    		return "";
    	}
    	StringBuffer s = new StringBuffer();
    	String startJql = field + " in (";
    	s.append(startJql);
    	for (String value : values) {
    		if (s.length() > startJql.length()) {
    			s.append(",");
    		}
    		if (field.equals("key")) {
    		    // jira bug - поиск при несуществующем ключе выдает ошибку (toLowerCase работает)
    		    s.append("'").append(field.toLowerCase()).append("'");
    		}
    		else {
        		s.append("'").append(value).append("'");
    		}	
    	}
    	s.append(")");
    	return s.toString();
    }
	
}
