package com.aplana.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

public abstract class AbstractRestClient {
	
    /*--------------------------------------------------------------- Members */
    private static final Logger logger = LoggerFactory.getLogger(AbstractRestClient.class);

    protected String jiraEndpoint = "http://localhost:8080";    
    protected boolean sslMode;
	
    /*------------------------------------------------------------------ Init */
	public void setJiraEndpoint(String jiraEndpoint) {
		this.jiraEndpoint = jiraEndpoint;
	}

	public void setSslMode(boolean sslMode) {
		this.sslMode = sslMode;
	}

	protected RestTemplate getRestTemplate() {
		RestTemplate template;
		if (sslMode) {
			logger.info("SSL Mode - true");

			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
			SSLContext sslContext;
			try {
//				String ksPassword = "changeit";
//				KeyStore keyStore = KeyStore.getInstance("JKS"); 
//				keyStore.load (new FileInputStream("/opt/atlassian/jira/key/jira.p12"), ksPassword.toCharArray());				
				sslContext = org.apache.http.ssl.SSLContexts.custom()
//		                        .loadKeyMaterial(keyStore, ksPassword.toCharArray())
						        .loadTrustMaterial(null, acceptingTrustStrategy)
						        .build();
			} 
			catch (/*CertificateException | IOException | UnrecoverableKeyException | */ KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
				throw new RuntimeException("Cannot set SSL context", e);
			}
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setHttpClient(httpClient);
			template = new RestTemplate(requestFactory);
		}
		else {
			logger.info("SSL Mode - false");
			template = new RestTemplate();
		}
		return template;
	}

}
