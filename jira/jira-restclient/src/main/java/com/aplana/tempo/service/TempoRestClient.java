package com.aplana.tempo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.aplana.jira.JiraConsts;
import com.aplana.service.AbstractRestClient;
import com.aplana.tempo.entity.Worklogs;

public class TempoRestClient extends AbstractRestClient {

    /*--------------------------------------------------------------- Members */
    private static final Logger logger = LoggerFactory.getLogger(TempoRestClient.class);
    
    private String apiToken;    

    /*------------------------------------------------------------------ Init */
    public void setJiraEndpoint(String jiraEndpoint) {
		this.jiraEndpoint = jiraEndpoint;
	}

	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}
	
	public void setSslMode(boolean sslMode) {
		this.sslMode = sslMode;
	}

	/*------------------------------------------------------------------ TaskDevPlan */
	public Worklogs getWorklogs(String issueKey, String dateFrom, String dateTo) {
    	logger.info("Issue [" + issueKey + "] - dateFrom [" + dateFrom + "] - dateTo [" + dateTo + "]");
    	RestTemplate template = getRestTemplate();
    	String url = jiraEndpoint + JiraConsts.TEMPO_URL_GET_WORKLOGS + "&tempoApiToken=" + apiToken + "&issueKey={issueKey}&dateFrom={dateFrom}&dateTo={dateTo}";
    	Worklogs worklogs = template.getForObject(url, Worklogs.class, issueKey,  dateFrom,  dateTo);
        return worklogs;
    }

}
