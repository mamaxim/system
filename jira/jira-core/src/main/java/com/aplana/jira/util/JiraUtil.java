package com.aplana.jira.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.type.EventDispatchOption;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.atlassian.query.Query;

/**
 * Служебные методы jira.
 * В том числе обеспечивают функциональность, которая отсутствует в стандартном Jira Rest Api.  
 */
public class JiraUtil {
	
    /*--------------------------------------------------------------- Members */
    private static final Logger logger = LoggerFactory.getLogger(JiraUtil.class);

    private static AttachmentManager attachmentManager = ComponentAccessor.getAttachmentManager();
    private static CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
    private static IssueManager issueManager = ComponentAccessor.getIssueManager();
    private static IssueService issueService = ComponentAccessor.getIssueService();
    private static OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
    private static PrioritySchemeManager prioritySchemaManager = ComponentAccessor.getComponent(PrioritySchemeManager.class);
    private static ProjectManager projectManager = ComponentAccessor.getProjectManager();
	private static ProjectRoleManager projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager.class);
    private static SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
	private static UserManager userManager = ComponentAccessor.getUserManager();

    /*------------------------------------------------------------ Attachment */
    public static List<Attachment> getAttachmets(Issue issue) {
        return attachmentManager.getAttachments(issue);
    }

    /*----------------------------------------------------------- CustomField */
    public static String getCustomFieldStringValue(Issue issue, long customFieldId) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        return (String) customField.getValue(issue);
    }
    
    public static Double getCustomFieldDoubleValue(Issue issue, long customFieldId) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        return (Double) customField.getValue(issue);
    }
    
    public static Long getCustomFieldLongValue(Issue issue, long customFieldId) {
    	Double value = getCustomFieldDoubleValue(issue, customFieldId); 
        return value == null ? null : value.longValue();
    }
    
    public static Date getCustomFieldDateValue(Issue issue, long customFieldId) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        return (Date) customField.getValue(issue);
    }

    public static String getCustomFieldOptionValue(Issue issue, long customFieldId) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        Option option = (Option) customField.getValue(issue);
        return option.getValue();
    }

    public static void updateCustomField(Issue issue, long customFieldId, String value) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        ModifiedValue<String> modifiedValue = new ModifiedValue<String>((String) issue.getCustomFieldValue(customField), value);
        customField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
    }

    public static void updateCustomField(Issue issue, long customFieldId, Double value) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        ModifiedValue<Double> modifiedValue = new ModifiedValue<Double>((Double) issue.getCustomFieldValue(customField), value);
        customField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
    }

    public static void updateCustomField(Issue issue, long customFieldId, Long value) {
    	Double val = value == null ? null : new Double(value);
    	updateCustomField(issue, customFieldId, val);
    }

    public static void updateCustomField(Issue issue, long customFieldId, Date value) {
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        ModifiedValue<Timestamp> modifiedValue = new ModifiedValue<Timestamp>((Timestamp) issue.getCustomFieldValue(customField), new Timestamp(value.getTime()));
        customField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
	}

    public static void updateOptionCustomField(Issue issue, long customFieldId, String value) {
    	Option newOption = null;
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
    	if (value != null)  {
    		FieldConfig relevantConfig = customField.getRelevantConfig(issue);
    		Options options = optionsManager.getOptions(relevantConfig);
//    		List<Option> options = optionsManager.getOptions(relevantConfig);
    		for (Option option : options) {
    			if (option.getValue().equals(value)) {
    				newOption = option;
    				break;
    			}
    		}
    		if (newOption == null)  {
    			throw new RuntimeException("Invalid option value: issue=" + issue.getKey() +"; option=" + value);
    		}
    	}
        ModifiedValue<Option> modifiedValue = new ModifiedValue<Option>((Option) issue.getCustomFieldValue(customField), newOption);
        customField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
    }

    @SuppressWarnings("unchecked")
	public static void updateMultiOptionCustomField(Issue issue, long customFieldId, Set<String> values) {
    	Set<Option> newOptions = new HashSet<>();
        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
    	if (values != null)  {
    		FieldConfig relevantConfig = customField.getRelevantConfig(issue);
    		Options options = optionsManager.getOptions(relevantConfig);
    		for (String value : values) {
    		    for (Option option : options) {
    			    if (option.getValue().equals(value)) {
    				    newOptions.add(option);
        			}
    		    }    
    		}
    	}
        ModifiedValue<Collection<Option>> modifiedValue = new ModifiedValue<Collection<Option>>((Collection<Option>) issue.getCustomFieldValue(customField), newOptions);
        customField.updateValue(null, issue, modifiedValue, new DefaultIssueChangeHolder());
    }

    /*---------------------------------------------------------------- Email */
	public static void sendEmail(Email email) {
		 SingleMailQueueItem smqi = new SingleMailQueueItem(email);
		 ComponentAccessor.getMailQueue().addItem(smqi);
	}  
	
   /*----------------------------------------------------------------- Issue */
	public static void reindexIssue(Issue issue) {
		ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
		MutableIssue mutableIssue = (MutableIssue) issue;
		String description = mutableIssue.getDescription();
		mutableIssue.setDescription(description == null ? " " : description + " ");
		issueManager.updateIssue(user, mutableIssue, EventDispatchOption.ISSUE_UPDATED, false);
		mutableIssue.setDescription(description);
		issueManager.updateIssue(user, mutableIssue, EventDispatchOption.ISSUE_UPDATED, false);
	}

	public static List<Issue> searchIssues(ApplicationUser user, String jql) {
    	List<Issue> issues = new ArrayList<Issue>();
        final SearchService.ParseResult parseResult = searchService.parseQuery(user, jql);
        if (parseResult.isValid()) {
            Query query = parseResult.getQuery();
        	logger.debug("searchIssues query: " + query.toString());
            try {
                final SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
                issues = results.getIssues();
            }
            catch (SearchException e) {
                logger.error("Error running searchIssues", e);
            }
        }
        else {
            logger.error("Error parsing jql: " + parseResult.getErrors());
        }
        return issues;
	}
    
    /*-------------------------------------------------------------- Priority */
    private static String getPriorityName(Priority priority) {
    	String name = priority.getNameTranslation();
    	if (name == null || name.isEmpty()) {
    		name = priority.getName();
    	}
		return name;
    }

	public static Collection<Priority> getProjectPpriorities(String projectKey) {
		Project project = projectManager.getProjectObjByKey(projectKey);
		FieldConfigScheme fieldConfigScheme = prioritySchemaManager.getScheme(project); 
		FieldConfig fieldConfig = prioritySchemaManager.getFieldConfigForDefaultMapping(fieldConfigScheme);        		
		Collection<Priority> priorities = prioritySchemaManager.getPrioritiesFromIds(prioritySchemaManager.getOptions(fieldConfig));
		return priorities;
    }

    public static void updatePriority(Issue issue, String projectKey, String priorityName) {
    	ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    	IssueInputParameters params = issueService.newIssueInputParameters();
		Collection<Priority> priorities = getProjectPpriorities(projectKey);
// global Collection<Priority> priorities = constManager.getPriorities();
    	for (Priority priority : priorities) {
    		if (getPriorityName(priority).equals(priorityName)) {
    	    	params.setPriorityId(priority.getId());
    			break;
    		}
    	}
    	if (params.getPriorityId() == null || params.getPriorityId().isEmpty()) {
    		throw new RuntimeException("Invalid priority: issue=" + issue.getKey() +"; priority=" + priorityName);
    	}
    	IssueService.UpdateValidationResult validationResult = issueService.validateUpdate(user, issue.getId(), params);
    	if(validationResult.isValid()) {
    	    IssueService.IssueResult updateResult = issueService.update(user, validationResult);
    	    if (!updateResult.isValid()) {
           		throw new RuntimeException("Cannot update: issue=" + issue.getKey() +"; priority=" + priorityName + "; errors=" + updateResult.getErrorCollection());
     	    }
    	}
    	else {
    		throw new RuntimeException("Cannot validate update: issue=" + issue.getKey() +"; priority=" + priorityName + "; errors=" + validationResult.getErrorCollection());
    	}    
    }
	
    /*------------------------------------------------------------------ Role */
	public static Set<ApplicationUser> getRoleUsers(String roleName, String projectKey) {
		ProjectRole role = projectRoleManager.getProjectRole(roleName);
		Project project = projectManager.getProjectObjByKey(projectKey);
		Set<ApplicationUser> users = projectRoleManager.getProjectRoleActors(role, project).getApplicationUsers();
		return users;
	}

	public static Collection<ProjectRole> getUserRoles(String username, String projectKey) {
		ApplicationUser user = userManager.getUserByName(username);
		Project project = projectManager.getProjectObjByKey(projectKey);
		return getUserRoles(user, project);
	}

	public static Collection<ProjectRole> getUserRoles(ApplicationUser user, Project project) {
		logger.debug("user = " + user + "; project = " + project);
		return projectRoleManager.getProjectRoles(user, project);
	}
	
}
