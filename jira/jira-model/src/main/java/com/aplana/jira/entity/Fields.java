package com.aplana.jira.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "fields")
@XmlAccessorType (XmlAccessType.FIELD)
public class Fields<T extends AbstractIssue> {
	
    /*--------------------------------------------------------------- Members */
	@XmlElement (name = "assignee")
	private Assignee assignee = null;

	@XmlElement (name = "created")
	private Date created = null;

	@XmlElement (name = "epic")
	private Epic epic = null;

	@XmlElement (name = "parent")
	private T parent = null;

	@XmlElement (name = "priority")
	private Priority priority = null;
	
	@XmlElement (name = "security")
	private Security security = null;
	
	@XmlElement (name = "status")
	private Status status = null;
	
	@XmlElement (name = "summary")
	private String summary = null;
	
	@XmlElement (name = "timeoriginalestimate")
	private Long timeoriginalestimate = null;

	/*---------------------------------------------------------------- Access */
	public Assignee getAssignee() {
		return assignee;
	}

	public void setAssignee(Assignee assignee) {
		this.assignee = assignee;
	}
	
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	public Epic getEpic() {
		return epic;
	}

	public void setEpic(Epic epic) {
		this.epic = epic;
	}

	public T getParent() {
		return parent;
	}

	public void setParent(T parent) {
		this.parent = parent;
	}
	
	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Security getSecurity() {
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Long getTimeoriginalestimate() {
		return timeoriginalestimate;
	}

	public void setTimeoriginalestimate(Long timeoriginalestimate) {
		this.timeoriginalestimate = timeoriginalestimate;
	}

}
