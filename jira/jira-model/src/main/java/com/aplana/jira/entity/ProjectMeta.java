package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "project-meta")
@XmlAccessorType (XmlAccessType.FIELD)
public class ProjectMeta {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "name")
    private String name;
	@XmlElement (name = "issuetypes")
	private List<IssueTypeMeta> issuetypes = new ArrayList<>();

    /*---------------------------------------------------------------- Access */
    public String getName() {
		return name;
	}
	
    public void setName(String name) {
		this.name = name;
	}

 	public List<IssueTypeMeta> getIssuetypes() {
		return issuetypes;
	}

	public void setIssuetypes(List<IssueTypeMeta> issuetypes) {
		this.issuetypes = issuetypes;
	}

}