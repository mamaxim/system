package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType (XmlAccessType.FIELD)
public abstract class AbstractIssue {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "id")
    private Long id;
    @XmlElement(name = "key")
    private String key;
	
    /*---------------------------------------------------------------- Access */
    public Long getId() {
		return id;
	}
	
    public void setId(Long id) {
		this.id = id;
	}

    public String getKey() {
		return key;
	}
	
    public void setKey(String key) {
		this.key = key;
	}

}