package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "meta")
@XmlAccessorType (XmlAccessType.FIELD)
public class Meta extends AbstractMeta {
	
    /*--------------------------------------------------------------- Members */
	@XmlElement (name = "projects")
	private List<ProjectMeta> projects = new ArrayList<>();

    /*---------------------------------------------------------------- Access */
 	public List<ProjectMeta> getProjects() {
		return projects;
	}

	public void setProjects(List<ProjectMeta> projects) {
		this.projects = projects;
	}

}