package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType (XmlAccessType.FIELD)
public abstract class AbstractSearchResults {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "maxResults")
	private String maxResults;
    @XmlElement (name = "total")
	private String total;

    /*---------------------------------------------------------------- Access */
	public String getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(String maxResults) {
		this.maxResults = maxResults;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}
