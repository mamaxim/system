package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "field-value")
@XmlAccessorType (XmlAccessType.FIELD)
public class FieldValue {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "id")
    private Long id;
    @XmlElement(name = "value")
    private String value;
	
    /*---------------------------------------------------------------- Access */
    public Long getId() {
		return id;
	}
	
    public void setId(Long id) {
		this.id = id;
	}

    public String getValue() {
		return value;
	}
	
    public void setValue(String value) {
		this.value = value;
	}

}