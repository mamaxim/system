package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "field-meta")
@XmlAccessorType (XmlAccessType.FIELD)
public class FieldMeta {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "name")
    private String name;
	@XmlElement (name = "allowedValues")
	private List<FieldValue> allowedValues = new ArrayList<>();

    /*---------------------------------------------------------------- Access */
    public String getName() {
		return name;
	}
	
    public void setName(String name) {
		this.name = name;
	}

 	public List<FieldValue> getAllowedValues() {
		return allowedValues;
	}

	public void setAllowedValues(List<FieldValue> allowedValues) {
		this.allowedValues = allowedValues;
	}

}