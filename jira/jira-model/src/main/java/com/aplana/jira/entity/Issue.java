package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "issue")
@XmlAccessorType (XmlAccessType.FIELD)
public class Issue extends AbstractIssue {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name="fields")
    private Fields<Issue> fields;

    /*---------------------------------------------------------------- Access */
	public Fields<Issue> getFields() {
		return fields;
	}

	public void setFields(Fields<Issue> fields) {
		this.fields = fields;
	}

}