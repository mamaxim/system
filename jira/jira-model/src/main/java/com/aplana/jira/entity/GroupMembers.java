package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="groupMembers")
@XmlAccessorType (XmlAccessType.FIELD)
public class GroupMembers {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "maxResults")
	private String maxResults;
    @XmlElement (name = "total")
	private String total;
    @XmlElement (name="values")
	private List<User> values = new ArrayList<User>();

    /*---------------------------------------------------------------- Access */
	public String getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(String maxResults) {
		this.maxResults = maxResults;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<User> getValues() {
		return values;
	}

	public void setValues(List<User> values) {
		this.values = values;
	}

}
