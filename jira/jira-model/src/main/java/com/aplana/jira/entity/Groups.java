package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="groups")
@XmlAccessorType (XmlAccessType.FIELD)
public class Groups {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "size")
	private int size;
    @XmlElement (name = "items")
	private List<Group> items = new ArrayList<Group>();

    /*---------------------------------------------------------------- Access */
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<Group> getItems() {
		return items;
	}

	public void setItems(List<Group> items) {
		this.items = items;
	}

}
