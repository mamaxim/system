package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "issuetype-meta")
@XmlAccessorType (XmlAccessType.FIELD)
public class IssueTypeMeta {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "name")
    private String name;
	@XmlElement (name = "fields")
	private FieldsMeta fields;

    /*---------------------------------------------------------------- Access */
    public String getName() {
		return name;
	}
	
    public void setName(String name) {
		this.name = name;
	}

 	public FieldsMeta getFields() {
		return fields;
	}

	public void setFields(FieldsMeta fields) {
		this.fields = fields;
	}

}