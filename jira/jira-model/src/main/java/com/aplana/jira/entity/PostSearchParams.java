package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "postSearchParams")
@XmlAccessorType (XmlAccessType.FIELD)
public class PostSearchParams {
		
	/*--------------------------------------------------------------- Members */
	@XmlElement (name="jql")
	private String jql;

	@XmlElement (name="startAt")
	private int startAt = 0;

	@XmlElement (name="maxResults")
	private int maxResults = 10000;

	@XmlElement (name = "fields")
	private List<String> fields = new ArrayList<>();
	
    /*------------------------------------------------------------------ Init */
	public PostSearchParams() {
	}

	public PostSearchParams(String jql, List<String> fields) {
		this.jql = jql;
		this.fields = fields;
	}
	
	/*---------------------------------------------------------------- Access */
	public String getJql() {
		return jql;
	}

	public void setJql(String jql) {
		this.jql = jql;
	}

	public int getStartAt() {
		return startAt;
	}

	public void setStartAt(int startAt) {
		this.startAt = startAt;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public void setFields(List<String> fields) {
		this.fields = fields;
	}

}
