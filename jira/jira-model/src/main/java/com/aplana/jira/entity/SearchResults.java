package com.aplana.jira.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="searchResults")
@XmlAccessorType (XmlAccessType.FIELD)
public class SearchResults extends AbstractSearchResults {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "issues")
	private List<Issue> issues = new ArrayList<>();

    /*---------------------------------------------------------------- Access */
 	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

}
