package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="user")
@XmlAccessorType (XmlAccessType.FIELD)
public class User {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "name")
	private String name;
    @XmlElement (name = "key")
	private String key;
    @XmlElement (name="emailAddress")
	private String email;
    @XmlElement (name="groups")
	private Groups groups;

    /*---------------------------------------------------------------- Access */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEmailAddress() {
		return email;
	}

	public void setEmailAddress(String email) {
		this.email = email;
	}

	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

}
