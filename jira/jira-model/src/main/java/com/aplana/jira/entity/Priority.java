package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="priority")
@XmlAccessorType (XmlAccessType.FIELD)
public class Priority {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "id")
	private Long id;
    @XmlElement (name = "name")
	private String name;

    /*---------------------------------------------------------------- Access */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
