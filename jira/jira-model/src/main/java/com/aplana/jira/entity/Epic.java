package com.aplana.jira.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="epic")
@XmlAccessorType (XmlAccessType.FIELD)
public class Epic {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement (name = "key")
	private String key;

    @XmlElement (name = "name")
	private String name;

    /*---------------------------------------------------------------- Access */
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
