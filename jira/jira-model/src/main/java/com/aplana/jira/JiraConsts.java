package com.aplana.jira;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class JiraConsts {

    /*------------------------------------------------------------- Constants */
	// jira
	public static final String JIRA_URL_REST = "/rest/api/2";
	public static final String JIRA_URL_GROUP_MEMBER = JIRA_URL_REST + "/group/member";
	public static final String JIRA_URL_ISSUE = JIRA_URL_REST + "/issue/";
	public static final String JIRA_URL_ISSUETYPE_META = JIRA_URL_ISSUE + "createmeta";
	public static final String JIRA_URL_SEARCH = JIRA_URL_REST + "/search";
	public static final String JIRA_URL_USER = JIRA_URL_REST + "/user";

	public static final String JIRA_REST_HEADER_AUTH = "Authorization";
	
	public static final String TIMETRACKING_FORMAT_MINUTES = "pretty";
	public static final String TIMETRACKING_FORMAT_HOURS = "hours";
	public static final String TIMETRACKING_FORMAT_DAYS = "days";

	public static final String JIRA_URL_AGILE = "/rest/agile/1.0";
	public static final String JIRA_URL_AGILE_ISSUE = JIRA_URL_AGILE + "/issue/";

	//jira fields
	public static final String JF_PRIORITY = "priority";
	
	// format
	public static final String FP_JQL_DATE = "yyyy-MM-dd";
	public static final DateFormat FORMAT_JQL_DATE = new SimpleDateFormat(FP_JQL_DATE);
	
	
	
	// tempo
	public static final String TEMPO_URL_REST = "/plugins/servlet/";
	public static final String TEMPO_URL_GET_WORKLOGS = TEMPO_URL_REST + "tempo-getWorklog/?format=xml";
	
    /*------------------------------------------------------------------ Init */
    private JiraConsts() {
    }
    
}
