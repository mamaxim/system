package com.aplana.tempo.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "worklogs")
@XmlAccessorType (XmlAccessType.FIELD)
public class Worklogs {
	
    @XmlElement(name="worklog")
    private List<Worklog> worklogs;

    /*---------------------------------------------------------------- Access */
    public List<Worklog> getWorklogs() {
		return worklogs;
	}

    /*--------------------------------------------------------------- Members */
    public void setWorklogs(List<Worklog> worklogs) {
		this.worklogs = worklogs;
	}
    
}
