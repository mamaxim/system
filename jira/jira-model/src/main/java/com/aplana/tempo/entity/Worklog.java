package com.aplana.tempo.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "worklog")
@XmlAccessorType (XmlAccessType.FIELD)
public class Worklog {
	
    /*--------------------------------------------------------------- Members */
    @XmlElement(name = "issue_key")
    private String issueKey;
    @XmlElement(name = "username")
    private String username;
    @XmlElement(name = "work_date")
    private String workDate;
    @XmlElement(name = "hours")
    private float hours;

    /*---------------------------------------------------------------- Access */
    public String getIssueKey() {
		return issueKey;
	}
	
    public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}
	
    public String getUsername() {
		return username;
	}
	
    public void setUsername(String username) {
		this.username = username;
	}
	
    public String getWorkDate() {
		return workDate;
	}
	
    public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}
	
    public float getHours() {
		return hours;
	}
	
    public void setHours(float hours) {
		this.hours = hours;
	}

}